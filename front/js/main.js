let searchBtn = document.querySelector('.providers-page__filter-search .search-btn')
let providersSearch = document.querySelector('.providers-page__filter-search .providers-search')
let closeBtn = document.querySelector('.providers-page__filter-search .close-btn')
let submitBtn = document.querySelector('.providers-page__filter-search .btn-submit')

searchBtn.addEventListener('click', () => {
    searchBtn.style.display = "none";
    providersSearch.style.display = "block";
    closeBtn.style.display = "block";
    submitBtn.style.display = "none";
});

closeBtn.addEventListener('click', () => {
    searchBtn.style.display = "flex";
    providersSearch.style.display = "none";
    closeBtn.style.display = "none";
});

let providersAll = document.querySelector('.providers-page__all')
let providersMore = document.querySelector('.providers-page__more')

providersMore.addEventListener('click', () => {
    providersAll.classList.toggle('toggle-list');
    providersMore.style.display = "none";
});